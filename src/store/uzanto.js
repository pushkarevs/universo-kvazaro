import graphqlClient from '../utils/client'
import { uzantoByObjIdQuery } from 'src/queries/queries'
import {
  changeSettings,
  forigoProjekto,
  changePassword,
  changeTelephoneNumberOrEmail,
  changeAvatar
} from 'src/queries/mutations'

export default {
  namespaced: true,
  state() {
    return {
      item: null,
      types: null
    }
  },
  getters: {
    getUzanto: (state) => (state.item ? state.item.uzanto : null)
  },
  mutations: {
    setUzanto: (state, payload) => (state.item = payload)
  },
  actions: {
    fetchUzanto({ commit }, { id }) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .query({
          query: uzantoByObjIdQuery,
          variables: { id },
          errorPolicy: 'all'
        })
        .then(({ data }) => {
          commit('setUzanto', data)
          this.dispatch('UIstore/hideLoading')
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading')
        })
    },
    changeUzanto(state, payload) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .mutate({
          mutation: changeSettings,
          variables: payload,
          update: () => {
            console.log('Вносим изменения')
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading')
          const resp = data.data.aplikiAgordoj
          this.dispatch('siriusoauth/fetchUser')

          return Promise.resolve(resp)
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading')
          console.error(error)
        })
    },
    changePassword(state, payload) {
      this.dispatch('UIstore/showLoading')
      console.log(payload)
      return graphqlClient
        .mutate({
          mutation: changePassword,
          variables: payload,
          update: () => {
            console.log('Вносим изменения')
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading')
          const resp = data.data.shanghiPasvorton
          // this.dispatch('siriusoauth/fetchUser');

          return Promise.resolve(resp)
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading')
          console.error(error)

          return Promise.resolve()
        })
    },

    changeTelephoneOrEmail(state, payload) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .mutate({
          mutation: changeTelephoneNumberOrEmail,
          variables: payload,
          update: () => {
            console.log('Вносим изменения')
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading')
          const resp = data.data.shanghiPoshtonTelefonon
          if (payload.hasOwnProperty('code')) {
            this.dispatch('siriusoauth/fetchUser')
          }
          return Promise.resolve(resp)
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading')
          console.error(error)
        })
    },
    changeAvatar(state, payload) {
      this.dispatch('UIstore/showLoading')
      // for (const k in payload) {
      //   console.log(k, payload[k]);
      // }
      return graphqlClient
        .mutate({
          mutation: changeAvatar,
          variables: payload,
          update: () => {
            console.log('Вносим изменения')
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading')
          const resp = data.data.instaliAvataron
          return Promise.resolve(resp)
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading')
          console.error(error)
        })
    },

    refetchUzanto({ commit }, { id }) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .query({
          query: uzantoByObjIdQuery,
          variables: { id },
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setUzanto', data)
          this.dispatch('UIstore/hideLoading')
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading')
        })
    }
  }
}
