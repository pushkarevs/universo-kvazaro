import graphqlClient from '../utils/client'
import { uzantojQuery } from 'src/queries/queries'

export default {
  namespaced: true,
  state() {
    return {
      list: null,
      edges: [],
      pageInfo: null
    }
  },
  getters: {
    getUzantoj: (state) => {
      return {
        pageInfo: state.pageInfo,
        edges: state.edges.map((item) => item[1])
      }
    }
  },
  mutations: {
    setUzantoj: (state, payload) => {
      let mapList
      mapList = new Map(state.edges)
      payload.uzantoj.edges.forEach((item) => {
        mapList.set(item.node.uuid, item)
      })
      state.pageInfo = payload.uzantoj.pageInfo
      state.edges = Array.from(mapList)
    }
  },
  actions: {
    fetchUzantoj({ commit }, { first, after }) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .query({
          query: uzantojQuery,
          variables: { first, after },
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setUzantoj', data)
          this.dispatch('UIstore/hideLoading')
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading')
        })
    }
  }
}
