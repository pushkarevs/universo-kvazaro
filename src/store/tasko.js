import graphqlClient from '../utils/client'
import { addTasko, arkTasko, forigoTasko, changeTasko } from 'src/queries/mutations'
import { projektoByUuidQuery, projektojTaskoNode } from 'src/queries/queries'

export default {
  namespaced: true,
  state() {
    return { tasko: null }
  },
  getters: {
    getTasko: (state) => (state.tasko ? state.tasko : null)
  },
  mutations: {
    setTasko: (state, payload) => {
      state.tasko = payload
    }
  },
  actions: {
    fetchTasko({ commit }, payload) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .query({
          query: projektojTaskoNode,
          variables: payload,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setTasko', data.projektojTasko.edges[0].node)
          this.dispatch('UIstore/hideLoading')
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading')
        })
    },

    addTasko({}, { nomo, kategorio, tipoId, statusoId, priskribo, projektoUuid }) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .mutate({
          mutation: addTasko,
          variables: {
            nomo,
            kategorio,
            tipoId,
            statusoId,
            priskribo,
            projektoUuid
          },
          update: () => {
            console.log('Регистрируем задачу')
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading')
          this.commit('projekto/setAddTasko', data.data.redaktuProjektojTaskoj.universoTaskoj)
          if (data?.data?.redaktuProjektojTaskoj?.status) {
            console.log(
              `Задача ${data.data.redaktuProjektojTaskoj.universoTaskoj?.uuid} успешно зарегистрирована.\nОтвет сервера: ${data.data.redaktuProjektojTaskoj.message}`
            )
          } else {
            console.log(
              `Задача не зарегистрирована. \nОшибка: ${
                data?.data?.redaktuProjektojTaskoj?.message ?? 'Неизвестно'
              }`
            )
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading')
          console.error(error)
        })
    },
    deleteTasko({ commit }, { id }) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .mutate({
          mutation: forigoTasko,
          variables: { id, forigo: true },
          update: () => {
            console.log('Удаляем задачу')
          }
        })
        .then((data) => {
          this.commit('projekto/setDelTasko', id)
          this.dispatch('UIstore/hideLoading')
          if (data?.data?.redaktuProjektojTaskoj?.status) {
            console.log(
              `Задача ${id} успешно удалена.\nОтвет сервера: ${data.data.redaktuProjektojTaskoj.message}`
            )
          } else {
            console.log(
              `Задача ${id} не удалена. \nОшибка: ${
                data?.data?.redaktuProjektojProjekto?.message ?? 'Неизвестно'
              }`
            )
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading')
          console.error(error)
        })
    },
    arkTasko({ commit }, { id }) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .mutate({
          mutation: arkTasko,
          variables: { id, forigo: true },
          update: () => {
            console.log('Архивируем задачу')
          }
        })
        .then((data) => {
          this.commit('projekto/setDelTasko', id)
          this.dispatch('UIstore/hideLoading')
          if (data?.data?.redaktuProjektojTaskoj?.status) {
            console.log(
              `Задача ${id} успешно архивирована.\nОтвет сервера: ${data.data.redaktuProjektojTaskoj.message}`
            )
          } else {
            console.log(
              `Задача ${id} не архивирована. \nОшибка: ${
                data?.data?.redaktuProjektojProjekto?.message ?? 'Неизвестно'
              }`
            )
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading')
          console.error(error)
        })
    },
    changeTasko({ commit }, payload) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .mutate({
          mutation: changeTasko,
          variables: payload,
          update: () => {
            console.log('Вносим изменения в задачу')
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading')
          if (data?.data?.redaktuProjektojTaskoj?.status) {
            commit('setTasko', data.data.redaktuProjektojTaskoj.universoTaskoj)

            console.log(
              `Задача ${payload.uuid} успешно изменена.\nОтвет сервера: ${data.data.redaktuProjektojTaskoj.message}`
            )
            return Promise.resolve(data.data.redaktuProjektojTaskoj.universoTaskoj)
          } else {
            console.log(
              `Задача ${payload.uuid} не изменена. \nОшибка: ${
                data?.data?.redaktuProjektojProjekto?.message ?? 'Неизвестно'
              }`
            )
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading')
          console.error(error)
        })
    }
  }
}
