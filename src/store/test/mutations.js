export function setTestMap(state, payload) {
  state.testMap.set(payload, payload)
  state.flag++
}

export function delTestMap(state, payload) {
  state.testMap.delete(payload)
  state.flag++
}
