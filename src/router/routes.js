const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Index.vue') },
      { path: '/gk', component: () => import('pages/komunumoj.vue') },
      { path: '/gp', component: () => import('pages/projektoj.vue') },
      { path: '/gu', component: () => import('pages/uzantoj.vue') },
      { path: '/kbase', component: () => import('pages/knowledgeBase.vue') },
      { path: '/registrado', component: () => import('pages/registrado') },
      { path: '/u:id', component: () => import('pages/uzanto') },
      { path: '/p:objId', component: () => import('pages/projekto') },
      { path: '/kp:id', component: () => import('pages/projektojKomunumo') },
      { path: '/k:id', component: () => import('pages/komunumo') },
      { path: '/p:objId/:taskoObjId', component: () => import('pages/tasko') },
      { path: '/agordoj', component: () => import('pages/uzantoAgordoj') },
      { path: '/restarigi', component: () => import('pages/restarigo.vue') },
      { path: '/test', component: () => import('pages/test') },
      // {path: '/test2', component: () => import('pages/test2')},

      {
        path: '/:catchAll(.*)*',
        component: () => import('pages/Error404.vue')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }

  // // Always leave this as last one,
  // // but you can also remove it
  // {
  //   path: '*',
  //   component: () => import('pages/Error404.vue'),
  // },
]

export default routes
