import { Node } from '@tiptap/core'

const Title = Node.create({
  name: 'title',
  group: 'block',
  content: 'inline*',
  parseHTML() {
    return [{ tag: 'h4' }]
  },
  renderHTML({ HTMLAttributes }) {
    return ['h4', HTMLAttributes, 0]
  }
})

export default Title
